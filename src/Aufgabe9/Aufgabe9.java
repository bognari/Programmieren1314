/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Diese Klasse ist für Aufgabe XX und macht: Blablabla ...
 *         (Aufgabentext oder kurze Beschreibung der Aufgabe usw. Also was es machen soll)
 */
public class Aufgabe9 {
    /**
     * Die normale Main Methode die zum Starten des Programms benötigt wird
     *
     * @param args Eingaben vom Aufruf
     */
    public static void main(String[] args) {
        System.out.printf("Variante mit for%n");
        // mit einer For - Schleife
        for (int i = 0; i < 3; i++) {
            System.out.printf("Erika Mustermann %n"
                    + "Musterstraße 123 %n"
                    + "97865 Musterdorf %n"
                    + "Telefon: 04243-5347 %n"
                    + "Handy: 0163-7357366 %n"
                    + "E-Mail: e.mustermann@provider.de %n%n");
        }
        System.out.printf("Variante mit while%n%n");
        int i = 0;
        int max = 3;
        // mit einer While - Schleife
        while (i < max) {
            System.out.printf("Erika Mustermann %n"
                    + "Musterstraße 123 %n"
                    + "97865 Musterdorf %n"
                    + "Telefon: 04243-5347 %n"
                    + "Handy: 0163-7357366 %n"
                    + "E-Mail: e.mustermann@provider.de %n%n");
            i++;
        }
        System.out.printf("Variante mit do while%n%n");
        i = 0;
        // mit einer Do - While - Schleife
        do {
            System.out.printf("Erika Mustermann %n"
                    + "Musterstraße 123 %n"
                    + "97865 Musterdorf %n"
                    + "Telefon: 04243-5347 %n"
                    + "Handy: 0163-7357366 %n"
                    + "E-Mail: e.mustermann@provider.de %n%n");
            i++;
        } while (i < max);
    }
}
