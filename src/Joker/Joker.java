public class Joker {
    /**
     * Gibt ein Array ohne doppelte Elemente zurück
     * @param as Array mit doppelten Elementen
     * @return Array ohne doppelte Elemente
     */
    public static int[] copyremove(int[] as) {
        int[] tmp = new int[as.length];
        int i = 0;

        // Untersucht alle Elemente aus as
        for (int a : as) {
            // ob sie noch nicht in tmp enthalten sind
            if (!isIn(tmp, a)) {
                // wenn nicht dann packe sie in tmp
                tmp[i] = a;
                i++;
            }
        }

        // in i steht die Anzahl an unterschiedlichen Elementen
        int[] ret = new int[i];
        // deswegen werden nur diese Stellen von tmp nach ret kopiert
        for (int j = 0; j < i; j++) {
            ret[j] = tmp[j];
        }

        return ret;
    }

    /**
     * Gibt an ob x in as enthalten ist
     * @param as das Array
     * @param x  das Element
     * @return true wenn x in as liegt, sonst false
     */
    private static boolean isIn(int[] as, int x) {
        for (int a : as ) {
            if (a == x) {
                return true;
            }
        }
        return false;
    }

    /**
     * Main halt
     * @param args wird nicht verwendet
     */
    public static void main(String[] args) {
        int[] as = new int[] {3, 5, 12, 6, 3, 4, 5, 12, 5, 3, 1, 6, 4, 5};
        int[] bs = copyremove(as);
        for (int b : bs) {
            System.out.println(b);
        }
    }
}
