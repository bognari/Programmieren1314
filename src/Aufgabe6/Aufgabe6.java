import java.util.Scanner;

/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Diese Klasse ist für Aufgabe XX und macht: Blablabla ...
 *         (Aufgabentext oder kurze Beschreibung der Aufgabe usw. Also was es machen soll)
 */
public class Aufgabe6 {
    /**
     * Die normale Main Methode die zum Starten des Programms benötigt wird
     *
     * @param args Eingaben vom Aufruf
     */
    public static void main(String[] args) {
        int h; // Die 3 Variablen müssen nur deklariert werden,
        int m; // da wir diese später definieren
        int nextIn;

        Scanner scanner = new Scanner(System.in); // Scanner erzeugen und den InputStream der Konsole übergeben
        System.out.printf("Wann soll der Zug ursprünglich ankommen?%n");
        System.out.printf("h: ");
        h = scanner.nextInt();  // hier definieren wir h
        System.out.printf("m: ");
        m = scanner.nextInt();
        System.out.printf("Um wie viele Minuten kommt der Zug zu spät? m: ");
        nextIn = scanner.nextInt();
        scanner.close();  // wenn man den Scanner nicht mehr braucht, sollte man ihn schließen.
        // sonst kann er nicht vom GC "abgeräumt" werden

        m += nextIn;

        /**
         * wir rechnen die Minuten in Stunden um
         */
        while (m >= 60) {
            m -= 60;
            h++;
        }

        /**
         * wir wandeln die Stunden in ein richtiges Format um
         */
        while (h >= 24) {
            h -= 24;
        }

        System.out.printf("Der Zug wird um %d:%d Uhr ankommen.", h, m);
    }
}
