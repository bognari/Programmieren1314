import java.util.Scanner;

/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Diese Klasse implemmentiert einfaches Patternmatching
 */
public class Match {

    /**
     * Prüft ob der String s gegen das Pattern(Muster) p gematcht werden kann
     *
     * @param p Pattern
     * @param s String
     * @return true wenn s mit p gematcht werden kann
     * @throws java.lang.IllegalArgumentException wenn s oder p verbotene Zeichen enthält
     */
    public static boolean match(String p, String s) {
        /*
            Wenn beide leer sind dann true
         */
        if (p.isEmpty() && s.isEmpty()) {
            return true;
        }
        /*
            wenn s leer und bei p ein * am Anfang steht dann lösche den Stern und prüfe erneut
         */
        if (s.isEmpty() && p.charAt(0) == '*') {
            return match(p.substring(1), s);
        }
        /*
            wenn s oder (xor) p leer ist dann false
         */
        if (p.isEmpty() ^ s.isEmpty()) {
            return false;
        }
        /*
            testen ob der Anfang von s oder p verbotene Zeichen enthält
         */
        if (! (p.charAt(0) == '*' || (p.charAt(0) >= '0' && p.charAt(0) <= '9')) ||
                ! (s.charAt(0) >= '0' && s.charAt(0) <= '9')) {
            throw new IllegalArgumentException("Eingaben sind falsch");
        }
        /*
            wenn s nicht leer ist und p ein * am Anfang hat, dann teste mit p und s (ohne den Anfang) oder lösche den *
            und teste mit s.
         */
        if (p.charAt(0) == '*') {
            return match(p, s.substring(1)) || match(p.substring(1), s);
        }
        /*
            wenn beide Anfänge gleich sind, dann teste für den Rest
            durch die Short-circuit Evaluation von && wird gleich false zurück gegeben wenn die Anfänge nicht passen.
         */
        return p.charAt(0) == s.charAt(0) && match(p.substring(1), s.substring(1));
    }


    /**
     * Die Main
     *
     * @param args wird nicht benutzt
     */
    public static void main(String[] args) {
        System.out.println(match("2*33", "233"));
        System.out.println(match("2*33", "24433"));
        System.out.println(match("1*", "24433"));
        System.out.println(match("1234", "1234"));
        System.out.println(match("*1234", "1234"));
        System.out.println(match("*4", "1234"));
        System.out.println(match("2*3*3", "233"));
        System.out.println(match("2*3**3", "233"));
        System.out.println(match("2*3**34", "233"));
        System.out.println(match("***", "3"));
        System.out.println(match("*", ""));
        System.out.println(match("3*3", "333"));
        System.out.println(match("1234**", "1234"));


        Scanner scanner = new Scanner(System.in);
        String p;
        String s;
        boolean run = true;
        do {
            System.out.print("Geben Sie das Pattern ein: ");
            p = scanner.nextLine();
            System.out.print("Geben Sie den String ein: ");
            s = scanner.nextLine();
            try {
                if (match(p, s)) {
                    System.out.printf("\"%s\" ist in \"%s\" enthalten%n", s, p);
                } else {
                    System.out.printf("\"%s\" ist in \"%s\" NICHT enthalten%n", s, p);
                }
            } catch (IllegalArgumentException iae) {
                System.err.println(iae.getMessage());
                run = false;
            }
        } while (run);
    }
}
