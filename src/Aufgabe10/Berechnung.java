/*
* Berechnet "7 + 11" -- Version mit Fehler
*/
public class Berechnung {  // class

    public static void main(String[] args) { // {
        int summe; // summe
        summe = 7 + 11; // 7 + 11 nicht 7 + 13
        System.out.println("7 + 11 ergibt"); // println
        System.out.println(summe); // ;
    }
} // }
