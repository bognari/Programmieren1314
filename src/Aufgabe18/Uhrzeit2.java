/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Diese Klasse ist für Aufgabe XX und macht: Blablabla ...
 *         (Aufgabentext oder kurze Beschreibung der Aufgabe usw. Also was es machen soll)
 */
public class Uhrzeit2 {
    /**
     * wie immer halt ...
     *
     * @param args [0] = die Zeitzone
     */
    public static void main(String[] args) {
        int input;

        try {   // müsst ihr noch nicht kennen, ist nur zum Abfangen von Fehlerfällen da
            input = Integer.parseInt(args[0]); // Einlesen des Argumentes und umwandeln in in
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
            // 1. Fehlerfall das Argument interessiert nicht
            // 2. das Argument ist keine int Zahl
            System.err.printf("Rufen sie das Programm auf mit \"java Uhrzeit2 [0-2]\"%n");
            // Schreibe das auf den Error Outputstream
            return;
        }

        if (input < - 12 || input > 12) {
            System.err.printf("%2d ist keine gültige Zeitzone.%n", input);
            return;
        }

        long t = System.currentTimeMillis();

        // die Stunden addieren
        t += input * 1000 * 60 * 60;

        // umwandeln in richtige Zeitformate
        int h = (int) (t / (1000 * 60 * 60)) % 24;
        int m = (int) (t / (1000 * 60)) % 60;
        int s = (int) (t / 1000) % 60;

        int[] mDay = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        String[] mName = {"Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September",
                "Oktober", "November", "Dezember"};
        String[] dName = {"Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"};

        int dIndex = ((int) t / (1000 * 60 * 60 * 24)) % 7;
        int d = (int) (t / (1000 * 60 * 60 * 24));
        int mIndex = 0;
        int y = 1970;

        // Jahre ausrechnen und auf Schaltjahre achten
        // die Sonderregel kann ignoriert werden (siehe Aufgabenstellung)
        while (d > 365 && ! (y % 4 == 0) || d > 366) {
            if (y % 4 == 0) {
                d -= 366;
                y++;
            } else {
                d -= 365;
                y++;
            }
        }

        // Monate ausrechnen und auf Schaltjahre achten
        // die Sonderregel kann ignoriert werden (siehe Aufgabenstellung)
        while (d > mDay[mIndex] && ! (mIndex == 1 && y % 4 == 0) || d >= mDay[mIndex]) {
            if (mIndex == 1 && y % 4 == 0) {
                d -= mDay[mIndex] + 1;
                mIndex++;
            } else {
                d -= mDay[mIndex];
                mIndex++;
            }
        }

        System.out.printf("Heute ist %s, der %2d. %s %4d.%n", dName[dIndex + 1], d + 1, mName[mIndex], y);

        switch (input) {
            case 0:
                System.out.printf("Es ist jetzt %2d:%2d Uhr und %2d Sekunden (UTC).%n", h, m, s);
                break;
            case 1:
                System.out.printf("Es ist jetzt %2d:%2d Uhr und %2d Sekunden (MEZ).%n", h, m, s);
                break;
            case 2:
                System.out.printf("Es ist jetzt %2d:%2d Uhr und %2d Sekunden (MESZ).%n", h, m, s);
                break;
            default:
                System.out.printf("Es ist jetzt %2d:%2d Uhr und %2d Sekunden.%n", h, m, s);
        }
    }
}
