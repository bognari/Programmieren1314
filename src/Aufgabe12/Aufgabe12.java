import java.util.Scanner;

/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Diese Klasse ist für Aufgabe XX und macht: Blablabla ...
 *         (Aufgabentext oder kurze Beschreibung der Aufgabe usw. Also was es machen soll)
 */
public class Aufgabe12 {
    /**
     * Die normale Main Methode die zum Starten des Programms benötigt wird
     *
     * @param args Eingaben vom Aufruf
     */
    public static void main(String[] args) {
        byte abgasnorm = 0; // muss definiert werden, da die Schleifen nicht immer
        // ausgeführt werden könnten (laut Compiler) deswegen auf 0 setzen
        int hubraum = 0;
        char typ = 0;
        float[] tableO = {15.13f, 7.36f, 6.75f};  // Array aus 3 Float Werten, wird als
        // Tabelle mit den Preisen gebraucht. Die Zählung beginnt bei 0.
        float[] tableD = {27.35f, 16.05f, 15.44f};


        Scanner scanner = new Scanner(System.in);

        while (abgasnorm < 1 || abgasnorm > 3) {  // Solange kein gültiger Wert verlange einen neuen
            System.out.printf("Bitte geben Sie die Abgasnorm ein: ");
            if (scanner.hasNextByte()) { // Blokiert bis ein Wert vom Inputstream
                // gelesen wurde. Wenn dieser kein Byte ist dann false.
                abgasnorm = scanner.nextByte(); // wenn es ein Byte ist dann lesen
            } else {
                scanner.next();                 // sonst verwerfen
            }
        }

        while (hubraum == 0) {
            System.out.printf("Bitte geben Sie den Hubraum ein: ");
            if (scanner.hasNextInt()) {
                hubraum = scanner.nextInt();
            } else {
                scanner.next();
            }
        }


        while (typ != 'D' && typ != 'O') {
            System.out.printf("Bitte geben Sie den Fahrzeugtyp ein: ");
            typ = scanner.next().charAt(0);
        }

        double tax;

        switch (typ) { // bei einem Switch wird in den Fall "gesprungen" der passt
            // (sonst default) und ab da abgearbeitet bis zu einem break.
            case 'D':
                tax = Math.ceil(hubraum / 100.0) * tableD[abgasnorm - 1];
                break; // ohne break würde er den default Fall immer mit abarbeiten und somit die Steuer überschreiben
            default:
                tax = Math.ceil(hubraum / 100.0) * tableO[abgasnorm - 1];
        }

        System.out.printf("Die Steuerschuld beträgt %.0f Euro pro Jahr.", Math.floor(tax));
        // %.0f bedeutet ein "Fließkommazahl" ohne Kommastellen.
    }
}
