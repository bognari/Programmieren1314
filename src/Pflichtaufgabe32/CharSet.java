/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Diese Klasse stellt Mengenoperationen da.
 */
public class CharSet {
    // Attribute
    /**
     * Der Inhalt
     */
    private char[] set; // Die Menge als Array
    /**
     * Die Größe
     */
    private int size; // Die Anzahl der Elemente der Menge
    // Konstruktoren

    /**
     * Leeremenge
     */
    public CharSet() {
        this.setSet(new char[0]);
    }

    /**
     * Erstellt eine Menge aus Werten
     *
     * @param set die Werte
     */
    public CharSet(char[] set) {
        this.setSet(set);
        this.vereinigung(new CharSet());
    }

    /**
     * Erstellt eine Menge aus einem String
     *
     * @param set die Menge als String
     */
    public CharSet(String set) {
        this.setSet(set.replaceAll(",|\\{|\\}", "").toCharArray());
        this.vereinigung(new CharSet());
    }

    // get-/set-Methoden

    /**
     * Getter für Set
     *
     * @return Inhalt
     */
    public char[] getSet() {
        return this.set;
    }

    /**
     * Getter für Size
     *
     * @return Größe
     */
    public int getSize() {
        return this.size;
    }

    /**
     * Setter für Set
     *
     * @param a die Werte
     */
    public void setSet(char[] a) {
        this.size = a.length;
        this.set = a;
    }

    // Überlagerung von Methoden der Klasse Object

    /**
     * Equals Methode
     *
     * @param x das zu vergleichende Objekt
     * @return true wenn A Teilmenge B und B Teilmenge A
     */
    public boolean equals(Object x) {
        if (x == null || ! (x instanceof CharSet)) {
            return false;
        }

        CharSet B = (CharSet) x;

        if (this.getSize() != B.getSize()) {
            return false;
        }

        // A Teilmenge B
        for (char a : this.set) {
            if (! B.isIn(a)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Testet ob eine Element in der Menge ist
     *
     * @param c das Element
     * @return true wenn das Element enthalten ist
     */
    private boolean isIn(char c) {
        for (char elem : this.set) {
            if (elem == c) {
                return true;
            }
        }
        return false;
    }

    /**
     * die toString
     *
     * @return die Menge als String
     */
    public String toString() {
        String elemente = "{";

        if (this.size > 0) {
            elemente = elemente + this.set[0];
        }

        for (int i = 1; i < this.size; i++) {
            elemente = elemente + "," + this.set[i];
        }
        elemente = elemente + "}";
        return elemente;
    }

    // Methoden zur Anwendung

    /**
     * Gibt eine neue Menge zurück, die der Durchschnitt von A und B ist
     *
     * @param c die Menge B
     * @return die neue Menge
     */
    public CharSet durchschnitt(CharSet c) {
        int j = 0;
        CharSet newSet = new CharSet();
        newSet.setSet(new char[this.size]);

        for (char a : this.set) {
            if (c.isIn(a) && ! newSet.isIn(a)) {
                newSet.set[j] = a;
                j++;
            }
        }

        char[] set = new char[j];

        for (int i = 0; i < set.length; i++) {
            set[i] = newSet.set[i];
        }

        newSet.setSet(set);

        return newSet;
    }

    /**
     * Gibt eine neue Menge zurück, die die Vereinigung von A und B ist
     *
     * @param c die Menge B
     * @return die neue Menge
     */
    public CharSet vereinigung(CharSet c) {
        int j = 0;
        CharSet newSet = new CharSet();
        newSet.setSet(new char[this.size + c.getSize()]);

        for (char a : this.set) {
            if (! newSet.isIn(a)) {
                newSet.getSet()[j++] = a;
            }
        }

        for (char b : c.set) {
            if (! newSet.isIn(b)) {
                newSet.getSet()[j++] = b;
            }
        }

        char[] set = new char[j];

        for (int i = 0; i < set.length; i++) {
            set[i] = newSet.set[i];
        }

        newSet.setSet(set);

        return newSet;
    }

    /**
     * Die Main Methode
     *
     * @param args args[0] = A, args[1] = B, args[2] = C
     */
    public static void main(String[] args) {
        CharSet aSet = new CharSet(args[0]);
        CharSet bSet = new CharSet(args[1]);
        CharSet cSet = new CharSet(args[2]);

        System.out.printf("Vereinigung der beiden ersten Mengen: %21s%n", aSet.vereinigung(bSet));
        System.out.printf("Durchschnitt der beiden ersten Mengen: %20s%n", aSet.durchschnitt(bSet));
        System.out.printf("Vereinigung der drei Mengen: %30s%n", aSet.vereinigung(bSet).vereinigung(cSet));
        System.out.printf("Durchschnitt der drei Mengen: %29s%n", aSet.durchschnitt(bSet).durchschnitt(cSet));
    }
}
