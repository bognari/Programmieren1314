/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Es soll die erste, zweite und dritte (alternierende)Quersumme einer Zahl berechnet werden.
 */
public class Quersummen {

    /**
     * Die Main Methode
     *
     * @param args in args[0] steht die zu untersuchende Zahl
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.printf("Rufen Sie das Programm wie folgt aus: java Quersummen 123456789");
            return;
        }

        System.out.printf("%-13s %s  %6d%n", "", "Quersumme erster Stufe:", getChecksum(getNumbers(args[0], 1), 1));
        System.out.printf("%-13s %s  %6d%n",
                "Alternierende", "Quersumme erster Stufe:", getChecksum(getNumbers(args[0], 1), - 1));
        System.out.printf("%-13s %s %6d%n", "", "Quersumme zweiter Stufe:", getChecksum(getNumbers(args[0], 2), 1));
        System.out.printf("%-13s %s %6d%n",
                "Alternierende", "Quersumme zweiter Stufe:", getChecksum(getNumbers(args[0], 2), - 1));
        System.out.printf("%-13s %s %6d%n", "", "Quersumme dritter Stufe:", getChecksum(getNumbers(args[0], 3), 1));
        System.out.printf("%-13s %s %6d%n",
                "Alternierende", "Quersumme dritter Stufe:", getChecksum(getNumbers(args[0], 3), - 1));
        System.out.println("#############################################");
        System.out.printf("%-13s %s  %6d%n", "", "Quersumme erster Stufe:", getChecksum(getNumbers2(args[0], 1), 1));
        System.out.printf("%-13s %s  %6d%n",
                "Alternierende", "Quersumme erster Stufe:", getChecksum(getNumbers2(args[0], 1), - 1));
        System.out.printf("%-13s %s %6d%n", "", "Quersumme zweiter Stufe:", getChecksum(getNumbers2(args[0], 2), 1));
        System.out.printf("%-13s %s %6d%n",
                "Alternierende", "Quersumme zweiter Stufe:", getChecksum(getNumbers2(args[0], 2), - 1));
        System.out.printf("%-13s %s %6d%n", "", "Quersumme dritter Stufe:", getChecksum(getNumbers2(args[0], 3), 1));
        System.out.printf("%-13s %s %6d%n",
                "Alternierende", "Quersumme dritter Stufe:", getChecksum(getNumbers2(args[0], 3), - 1));
        System.out.println("#############################################");
        System.out.printf("%-13s %s  %6d%n", "", "Quersumme erster Stufe:", getChecksum2(args[0], 1, 1));
        System.out.printf("%-13s %s  %6d%n",
                "Alternierende", "Quersumme erster Stufe:", getChecksum2(args[0], 1, - 1));
        System.out.printf("%-13s %s %6d%n", "", "Quersumme zweiter Stufe:", getChecksum2(args[0], 2, 1));
        System.out.printf("%-13s %s %6d%n",
                "Alternierende", "Quersumme zweiter Stufe:", getChecksum2(args[0], 2, - 1));
        System.out.printf("%-13s %s %6d%n", "", "Quersumme dritter Stufe:", getChecksum2(args[0], 3, 1));
        System.out.printf("%-13s %s %6d%n",
                "Alternierende", "Quersumme dritter Stufe:", getChecksum2(args[0], 3, - 1));
    }

    /**
     * Diese Methode Wandelt eine Zahl in ein Array von Integer Teilzahlen um
     * mit String
     *
     * @param number Zahl als String
     * @param size   Größe der Teilzahlen
     * @return Array von Teilzahlen
     */
    private static int[] getNumbers(String number, int size) {
        int[] ret = new int[(int) Math.ceil(number.length() / (double) size)];
        int end = number.length();
        for (int i = 0; end > 0; i++, end -= size) {
            ret[i] = Integer.parseInt(number.substring(end - size < 0 ? 0 : end - size, end));
        }
        return ret;
    }

    /**
     * Diese Methode Wandelt eine Zahl in ein Array von Integer Teilzahlen um
     * mathematisch
     *
     * @param number Zahl als String
     * @param size   Größe der Teilzahlen
     * @return Array von Teilzahlen
     */
    private static int[] getNumbers2(String number, int size) {
        int num = Integer.parseInt(number);
        int[] ret = new int[(int) Math.ceil(Math.log10(num))];
        for (int i = 0; num > 0; i++) {
            ret[i] = num % (int) Math.pow(10, size);
            num /= Math.pow(10, size);
        }
        return ret;
    }

    /**
     * Diese Methode berechnet aus einem Int Array die gewollte Quersumme
     *
     * @param numbers das Int Array
     * @param factor  1 = Quersumme, -1 = alternierende Quersumme
     * @return die Quersumme des Arrays
     */
    private static int getChecksum(int[] numbers, int factor) {
        int ret = 0;
        for (int i = 0; i < numbers.length; i++) {
            ret += Math.pow(factor, i) * numbers[i];
        }
        return ret;
    }

    /**
     * diese Methode bestimmt mathematisch die gewünschte Quersumme einer Zahl
     *
     * @param number die Zahl als String
     * @param size   größe der Teilzahlen
     * @param factor 1 = Quersumme, -1 = alternierende Quersumme
     * @return die Quersumme
     */
    private static int getChecksum2(String number, int size, int factor) {
        int num = Integer.parseInt(number);
        int ret = 0;
        for (int i = 0; num > 0; i++) {
            ret += (num % Math.pow(10, size)) * Math.pow(factor, i);
            num /= Math.pow(10, size);
        }
        return ret;
    }
}
