import java.util.Scanner;

/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Es soll die Differenz zwischen zwei Datumsangaben in Tagen errechnet werden
 */
public class Pflichtaufgabe19 {
    /**
     * Die Main Methode
     *
     * @param args wird nicht verwendet
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        /*
         * Das Datum als Int Array der Form: Tag Monat Jahr
         */
        int[] date1 = new int[3];
        int[] date2 = new int[3];

        System.out.printf("Geben Sie das erste Datum ein: ");
        for (int i = 0; i < 3; i++) {
            date1[i] = scanner.nextInt();
        }

        System.out.printf("Geben Sie das zweite Datum ein: ");
        for (int i = 0; i < 3; i++) {
            date2[i] = scanner.nextInt();
        }

        System.out.printf("Tage: %d %n", (days1(date2) - days1(date1)));
    }

    /**
     * Diese statische Methode gibt die vergangenen Tage seit dem 1.1.1901 zurück
     *
     * @param date Das Datum
     * @return Anzahl der vergangenen Tage
     */
    public static int days1(int[] date) {
        int days = 0;
        int y = 1901;
        int m = 1;
        int d = 1;
        int[] mDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        while (y < date[2]) {
            days += (y % 4 == 0) ? 366 : 365;
            y++;
        }

        while (m < date[1]) {
            days += ((m == 2) && (y % 4 == 0)) ? 29 : mDays[m - 1];
            m++;
        }

        return days + (date[0] - d);
    }
}
