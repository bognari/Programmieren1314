import java.util.Scanner;

/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Diese Klasse ist für Aufgabe XX und macht: Blablabla ...
 *         (Aufgabentext oder kurze Beschreibung der Aufgabe usw. Also was es machen soll)
 */
public class Aufgabe8 {
    /**
     * Die normale Main Methode die zum Starten des Programms benötigt wird
     *
     * @param args Eingaben vom Aufruf
     */
    public static void main(String[] args) {
        System.out.printf("Dieses Programm soll die pq-Formel berechnen.%n");

        double a, b, c; // wieder nur Deklaration der 3 Variablen

        Scanner scanner = new Scanner(System.in);

        System.out.printf("Geben sie a ein. a: ");
        a = scanner.nextDouble(); // durch das Belegen mit einem Wert, wird sie erst definiert
        System.out.printf("Geben sie b ein. b: ");
        b = scanner.nextDouble();
        System.out.printf("Geben sie c ein. c: ");
        c = scanner.nextDouble();
        System.out.printf("Die Gleichung lautet: %.2f * x^2 + %.2f * x + %.2f.%n", a, b, c);

        if (a == 0) {  // testen ob a != 0 ist.
            System.err.printf("Die pq-Formel ist nur für a!=0 definiert.");
            return;
        }

        double p = b / a;  // hier werden die Variablen gleich Definiert und nicht nur Deklariert
        double q = c / a;
        double tmp = Math.pow(p / 2.0, 2.0) - q;

        if (tmp < 0) {
            System.err.printf("Mit der pq-Formel konnte keine Nullstelle berechnet "
                    + "werden, da unter der Wurzel %.2f steht.", tmp);
            return;
        }

        double x1 = - (p / 2.0) + Math.sqrt(tmp);
        double x2 = - (p / 2.0) - Math.sqrt(tmp);
        System.out.printf("Die Nullstellen sind, x1 = %.2f und x2 = %.2f", x1, x2);
    }
}
