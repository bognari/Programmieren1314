public class Kreis extends Figur {

    private final double r;

    private static int nummer = 0;

    public Kreis(double r) {
        super("Kreis " + nummer++);
        this.r = r;
    }

    @Override
    public double getFlaecheninhalt() {
        return Math.PI * Math.pow(r, 2);
    }

    @Override
    public double getUmfang() {
        return 2 * Math.PI * r;
    }

    @Override
    public double[] getSeiten() {
        double[] ret = {r};
        return ret;
    }
}
