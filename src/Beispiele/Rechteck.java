public class Rechteck extends Figur {

    private final double a;
    private final double b;

    private static int nummer = 0;

    public Rechteck(double a, double b) {
        super("Rechteck " + nummer++);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getFlaecheninhalt() {
        return a * b;
    }

    @Override
    public double getUmfang() {
        return 2 * (a + b);
    }

    @Override
    public double[] getSeiten() {
        double[] ret = {a, b};
        return ret;
    }
}
