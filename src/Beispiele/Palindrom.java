/**
 * Created by stephan on 05.03.14.
 */
public class Palindrom {

    public static void main(String[] args) {
        int[] a = {1234, 2332, -1, -101, 987789};

        palindrom(a);
        System.out.println("##########################");

        palindrom2(a);
    }

    public static void palindrom(int[] a) {
        if (a == null || a.length == 0) {
            new IllegalArgumentException();
        }

        for (int i : a) {
            if (String.valueOf(Math.abs(i)).equals(new StringBuilder(String.valueOf(Math.abs(i))).reverse().toString())) {
                System.out.println(i);
            }
        }
    }

    public static void palindrom2(int[] a) {
        if (a == null || a.length == 0) {
            new IllegalArgumentException();
        }
        for (int i : a) { // for (int l = 0; l < a.length(); l++) {int i = a[l]...}
            char[] c = String.valueOf(Math.abs(i)).toCharArray();
            boolean isPalindrom = true;

            for (int j = 0; j < c.length; j++) {
                if (c[j] != c[c.length - 1 - j]) {
                    isPalindrom = false;
                    break;
                }
            }
            if (isPalindrom) {
                System.out.println(i);
            }
        }
    }
}
