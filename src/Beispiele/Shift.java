/**
 * Created by stephan on 10.03.14.
 */
public class Shift {
    static void shiftArray(int[] a, int k) {
        if (k < 0) {
            k = a.length + (k % a.length);
        } else {
            k = k % a.length;
        }

        int[] b = new int[a.length];

        for(int i = 0; i < a.length; i++) {
            b[(i + k) % a.length] = a[i];
        }

        for(int i = 0; i < b.length; i++) {
            a[i] = b[i];
        }
    }

    static void nora(int[] a, int k) {
        if (k % a.length == 0) return; // nur return nicht return a
        int[] b = new int[a.length]; // new int nicht b
        if (Math.abs(k) >= a.length && k >= 0) k = k % a.length; // |k| ist Math.abs(k)
        if (Math.abs(k) >= a.length && k < 0) k = -(k % a.length);
        if (k >= 0) {
            int j = 0; // hier das j hin
            for (int i = k + 1; i < b.length; i++) { // ; statt ,
                b[j] = a[i];
                j++;
            }
            for (int i = 0; i <= k; i++) { // ; statt , und ++ statt --
                b[j] = a[i];
                j++;
            }
        }
        // return ist hier blöd
        if (k < 0) {
            int j = 0; // hier das j hin
            for (int i = Math.abs(k); i < a.length; i++) { // ; statt , dann wieder Math.abs usw
                b[j] = a[i];
                j++;
            }
            for (int i = 0; i < Math.abs(k); i++) { // ; statt , und ++ statt --
                b[j] = a[i];
                j++;
            }
        }
        // hier copy von b auf a
        for(int i = 0; i < b.length; i++) {
            a[i] = b[i];
        }
    }

    static void ungradeZuErst(int[] a) {
        if (a == null || a.length == 0)
            throw new IllegalArgumentException(); //  hier throw

        int[] b = new int[a.length];

        int start = 0;
        int bende = a.length - 1;
        int bstart = 0;

        while (start < a.length) { // kein do
            if (a[start] % 2 == 0) {
                b[bende] = a[start];
                start++;
                bende--;
            } else {
                b[bstart] = a[start];
                start++;
                bstart++;
            }
        }
        for (int i = 0; i < a.length; i++) { // hier geht die foreach nicht !
            // da dort kein element verändert werden darf, und das i bei dir war ein element und kein index
            a[i] = b[i];
        }
    }

    static void uZ(int[] a) {
        if (a == null || a.length == 0)
            throw new IllegalArgumentException();

        int[] b = new int[a.length];
        int j = 0;

        for (int i = 0; i < a.length; i++)
            if (a[i] % 2 == 1)
                b[j++] = a[i];

        for (int i = 0; i < a.length; i++)
            if (a[i] % 2 == 0)
                b[j++] = a[i];

        for (int i = 0; i < a.length; i++)
            a[i] = b[i];
    }



    public static void main(String[] args) {
        int[] a = {0, 1, 2, 3, 4, 5, 6};

        //nora(a, -3);
        //ungradeZuErst(a);

        uZ(a);

        for (int z : a) {
            System.out.println(z);
        }

    }
}
