import java.util.Arrays;
import java.util.Random;

public abstract class Figur {
    private final String name;

    public Figur(String name) {
        this.name = name;
    }

    public final String getName() {
        return this.name;
    }

    public abstract double getFlaecheninhalt();

    public abstract double getUmfang();

    public abstract double[] getSeiten();


    public static void main(String[] args) {
        Figur[] figuren = new Figur[10];

        Random r = new Random();

        /*
            Figuren erstellen
         */

        for (int i = 0; i < figuren.length; i++) {
            switch (r.nextInt(2)) {
                case 0:
                    figuren[i] = new Kreis(r.nextInt(10) + 1);
                    break;
                case 1:
                    figuren[i] = new Rechteck(r.nextInt(10) + 1, r.nextInt(10) + 1);
            }
        }

        /*
            Figuren ausgeben
         */

        for (Figur figur : figuren) {
            System.out.println("#########################");
            System.out.println(figur.getName());
            System.out.println(Arrays.toString(figur.getSeiten()));
            System.out.printf("Flächeninhalt %.3f%n", figur.getFlaecheninhalt());
            System.out.printf("Umfang %.3f%n", figur.getUmfang());
            System.out.println();
        }


    }
}
