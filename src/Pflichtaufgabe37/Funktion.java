/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Dies ist die abstrakte Klasse für Funktionen
 *         Hier werden die "Funktionen" f und g nur angekündigt.
 *         Implemmentiert werden diese erst in den Unterklassen.
 */
public abstract class Funktion {
    /**
     * Startwert für die Tabelle
     */
    private static double min;
    /**
     * Endwert für die Tabelle
     */
    private static double max;
    /**
     * Schrittwert für die Tabelle
     */
    private static double step;

    /**
     * Die Epsilonumgebung für das Newtonverfahren
     */
    private static double eps = 0.0000000001;

    /**
     * Berechnet y von f(x)
     *
     * @param x der Parameter
     * @return y
     */
    public abstract double f(double x);

    /**
     * Berechnet y von f'(x) = g(x)
     *
     * @param x der Parameter
     * @return y
     */
    public abstract double g(double x);

    /**
     * Setzt die Einstellungen
     *
     * @param min  Startwert
     * @param max  Endwert
     * @param step Schrittwert
     */
    public static void setSettings(double min, double max, double step) {
        Funktion.min = min;
        Funktion.max = max;
        Funktion.step = step;
    }

    /**
     * Gibt die Wertetabelle aus
     */
    public void tabelle() {
        for (double x = min; x <= max + step; x += step) {
            System.out.printf("f(%.1f) = %12.8f%n", x, this.f(x));
        }
    }

    /**
     * Berechnet die Nullstellen mittels Newtonverfahren
     *
     * @param x Startwert
     */
    public void newton(double x) {
        do {
            x -= this.f(x) / this.g(x);
        } while (Math.abs(this.f(x)) > eps);
        System.out.printf("x = %12.8f%n", x);
    }

    /**
     * Die Main halt
     *
     * @param args wird nicht benutzt
     */
    public static void main(String[] args) {
        Funktion.setSettings(1, 2, 0.1);
        Funktion funktion = new Kubik(2, - 20, - 6, 30);
        funktion.tabelle();
        System.out.println();
        funktion.newton(Integer.MAX_VALUE);
        funktion.newton(Integer.MIN_VALUE);
        funktion.newton(0);
        System.out.println();
        System.out.printf("f(%.8f) = %12.18f%n", 10.1499672542564060, funktion.f(10.1499672542564060));
        System.out.printf("f(%.8f) = %12.18f%n", - 1.2929572821897817, funktion.f(- 1.2929572821897817));
        System.out.printf("f(%.8f) = %12.18f%n", 1.14299002793337840, funktion.f(1.14299002793337840));
    }
}
