/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Diese Klasse ist für kubische Funktionen gedacht
 */
public class Kubik extends Parabel {

    /**
     * Faktor a
     */
    private double a;

    /**
     * Erstellt eine kubische Funktion
     *
     * @param a Faktor a
     * @param b Faktor b
     * @param c Faktor c
     * @param d Faktor d
     */
    public Kubik(double a, double b, double c, double d) {
        super(b, c, d);
        this.a = a;
    }

    @Override
    public double f(double x) {
        return a * Math.pow(x, 3) + super.f(x);
    }

    @Override
    public double g(double x) {
        return 3 * a * Math.pow(x, 2) + super.g(x);
    }
}
