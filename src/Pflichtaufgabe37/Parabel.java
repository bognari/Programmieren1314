/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Diese Klasse ist für quadratische Funktionen gedacht
 */
public class Parabel extends Funktion {

    /**
     * Faktor a
     */
    private double a;
    /**
     * Faktor b
     */
    private double b;
    /**
     * Fakor c
     */
    private double c;

    /**
     * Erstellt eine quadratische Funktion
     *
     * @param a Faktor a
     * @param b Faktor b
     * @param c Faktor c
     */
    public Parabel(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }


    @Override
    public double f(double x) {
        return a * Math.pow(x, 2) + b * x + c;
    }

    @Override
    public double g(double x) {
        return 2 * a * x + b;
    }
}
