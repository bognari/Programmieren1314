/**
 * @author Vorname Name 1234567 Gruppe 4a
 * @author Vorname Name 1234567 Gruppe 4a
 *         <p>
 *         Diese Klasse ist für Aufgabe XX und macht: Blablabla ...
 *         (Aufgabentext oder kurze Beschreibung der Aufgabe usw. Also was es machen soll)
 */
public class Uhrzeit {
    /**
     * wie immer halt ...
     *
     * @param args [0] = die Zeitzone
     */
    public static void main(String[] args) {
        int input;
        try {   // müsst ihr noch nicht kennen, ist nur zum Abfangen von Fehlerfällen da
            input = Integer.parseInt(args[0]); // Einlesen des Argumentes und umwandeln in in
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
            // 1. Fehlerfall das Argument interessiert nicht
            // 2. das Argument ist keine int Zahl
            System.err.printf("Rufen sie das Programm auf mit \"java Uhrzeit2 [0-2]\"%n");
            // Schreibe das auf den Error Outputstream
            return;
        }

        if (input < - 12 || input > 12) {
            System.err.printf("%2d ist keine gültige Zeitzone.%n", input);
            return;
        }

        long t = System.currentTimeMillis();

        // die Stunden addieren
        t += input * 1000 * 60 * 60;

        // umwandeln in richtige Zeitformate
        int h = (int) (t / (1000 * 60 * 60)) % 24;
        int m = (int) (t / (1000 * 60)) % 60;
        int s = (int) (t / 1000) % 60;

        switch (input) {
            case 0:
                System.out.printf("Es ist jetzt %2d:%2d Uhr und %2d Sekunden (UTC).%n", h, m, s);
                break;
            case 1:
                System.out.printf("Es ist jetzt %2d:%2d Uhr und %2d Sekunden (MEZ).%n", h, m, s);
                break;
            case 2:
                System.out.printf("Es ist jetzt %2d:%2d Uhr und %2d Sekunden (MESZ).%n", h, m, s);
                break;
            default:
                System.out.printf("Es ist jetzt %2d:%2d Uhr und %2d Sekunden.%n", h, m, s);
        }
    }
}
