/**
 *
 */
public class Klausur120314 {

    /**
     * Sortiert nach BubbleSort
     * @param b Array zum sortieren
     */
    static void bubbleSort(int[] b) {
        for (int i = 0; i < b.length - 1; i++) {
            for (int j = 0; j < b.length - 1; j++) {
                if (!comp(b[j], b[j + 1])) {
                    int tmp = b[j];
                    b[j] = b[j + 1];
                    b[j + 1] = tmp;
                }
            }
        }
    }

    /**
     * Sortier Bedingung nach der Aufgabe
     * @param a der Vorgaenger
     * @param b der Nachfolger
     * @return true wenn die Bedingung erfuellt ist, sonst false
     */
    public static boolean comp(int a, int b) {
        if (Math.abs(a) == Math.abs(b)) {
            return a < b;
        }
        return Math.abs(a) < Math.abs(b);
    }

    public static void specialSort(int[] a) {
        if (a == null || a.length == 0) {
            throw new IllegalArgumentException();
        }
        bubbleSort(a);
    }

    public static void main(String[] args) {
        int[] a = {5, 2, -5, 1, 3, -5, -2, 4, 0};
        specialSort(a);
        for(int i : a)
            System.out.println(i);
    }
}
