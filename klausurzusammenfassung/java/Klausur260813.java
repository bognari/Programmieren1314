/**
 *
 */
public class Klausur260813 {

    /**
     * Sortiert nach BubbleSort (aus Aufgabe 1)
     * @param b Array zum sortieren
     */
    static void bs(int[] b) {
        for (int j = 1; j < b.length; j++) {
            int k = b[j],
                i = j - 1;
            while (i >= 0 && b[i] > k) {
                b[i + 1] = b[i];
                i--;
            }
            b[i + 1] = k;
        }
    }

    /**
     * Testet ob Elemente doppelt vorhanden sind in einem sortieren Array
     * @param b sortiertes Array
     * @return
     */
    static boolean isSet(int[] b) {
        int p = b[0];
        for (int i = 1; i < b.length; i++) {
            // testet on das Element gleich dem Vorgaenger ist
            if (p == b[i])
                return false;
            p = b[i];
        }
        return true;
    }

    /**
     * Gibt des Index des Wertes zurueck
     * @param b sortiertes Array
     * @param x gesuchter Wert
     * @return -1 wenn nicht enthalten
     */
    static int getIndex(int[] b, int x) {
        for (int i = 0; i < b.length; i++)
            // Testet ob es das gesuchte Element ist
            if (b[i] == x)
                return i;
        return -1;
    }

    /**
     * erstellt ein neues Array
     * @param a Quelle des neuen Arrays
     * @return
     */
    static int[] copy(int[] a) {
        int[] b = new int[a.length];
        for (int i = 0; i < b.length; i++)
            b[i] = a[i];
        return b;
    }

    /**
     * Gibt das das nachfolge Element von x zurueck, oder wenn es nicht existiert dann x.
     * Wenn die Folge leer, Null, doppelte Elemente oder x nicht enthaelt wird eine IllegalArgumentException ausgeloest.
     * @param a die Folge
     * @param x das Element
     * @return
     */
    static int next(int[] a, int x) {
        // Null und Leere "Folgen" abfangen
        if (a == null || a.length == 0)
            throw new IllegalArgumentException();
        int[] b = copy(a);
        bs(b);
        int i = getIndex(b, x);
        // Mehrmaliges Vorkommen und nicht enhtalten von x Abfangen
        if (!isSet(b) || i < 0)
            throw new IllegalArgumentException();
        return i < b.length - 1 ? b[i + 1] : b[i];
    }

    public static void main(String[] args) {
        int[] a = {12, 7, 5, 9, 13, 24};
        int x = 5;
        int ret = next(a, x);
        System.out.print(ret);
    }
}
