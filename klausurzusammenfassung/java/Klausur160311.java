
public class Klausur160311 {
  /**
   * Ermittelt das Mini- und Maximum eines Arrays
   * 
   * @param a das Eingabe Array
   * @return {Minumus, Maximum}
   */
  static int[] minMax(int[] a) {
    int min = a[0]; 
    int max = a[0];
    for (int i = 1; i < a.length; i++) {
    /*
     * ganz normale Mini- und Maximum Berechnung
     * mit kurzschreibweise fuer if
     */
      min = a[i] < min ? a[i] : min;
        //  wenn   ? dann : sonst
      max = a[i] > max ? a[i] : max;
    }
    return new int[] {min, max};
  }
    
  /**
   * Erzeugt ein Array aus einer Vorkommensliste, bei dem 
   * die Vorkommen (Anzahl) = e sind
   *
   * @param a die Vorkommensliste
   * @param e die vergleichszahl
   * @return Array von Keys fuer die gilt: a[i] == e
   */
  static int[] gleicheWerte(int[] a, int e) {
    // neues Rueckgabe Array, benoetigte Laenge ist maximal a.length
    int[] ret = new int[a.length]; 
    int   j = 0; // Index des Rueckgabe Arrays
    for (int i = 1; i < a.length; i++) 
      if (a[i] == e)
        ret[j++] = i; // Wenn a[i] == e, dann packe i in ret
    return ret;
  }
    
  static void modus(int[] a) {
    // fange die Fehlerfaelle aus der Aufgabe ab
    if (a == null || a.length == 0) throw new IllegalArgumentException();
    
    /*
     * Erstelle ein neues Array, das alle Werte von a als Key aufnehmen kann
     * Da a auch negative Werte enthalten darf, benutzen wir das Minimum von 
     * a als Offset ( - (-x) -> + x)
     * die +2 ergibt sich aus: +1 weil das Array sonst zu klein waere 
     * new int[3] = a[0] bis a[2] und 
     * die zweite +1 damit wir die 0 als Wert von a mit der 0 der Default 
     * Belegung von vorkommensListe unterscheiden koennen
     */
    int[] vorkommensListe = new int[(minMax(a)[1] - minMax(a)[0]) + 2];
    for (int i = 0; i < a.length; i++)
      /*
       * trage die Anzahl der vorkommen der Values in werte ein
       * die +1 ist die zweite +1 von Oben
       */
      vorkommensListe[a[i] - minMax(a)[0] + 1]++; 
    
    /*
     * hole alle Keys von werte mit werte[i] == max(werte) 
     */
    vorkommensListe = gleicheWerte(vorkommensListe, minMax(vorkommensListe)[1]);
    
    String s = "Modus:";
    for (int i = 0; i < vorkommensListe.length; i++) 
      /*
       * Gibt alle "benutzten" Stellen des Arrays aus und 
       * wandelt diese wieder in die alten Zahlen um
       */
      s = vorkommensListe[i] > 0 ? s + " " + 
       (vorkommensListe[i] + minMax(a)[0] - 1) : s;
    System.out.println(s);
  }
  
  public static void main(String[] args) {
    int[] folge = {3,3,4,12,4,3,4,0,1};
    modus(folge);
  }
}
