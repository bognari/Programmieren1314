
public class Klausur060313 {
  /**
   * Testet ob in Array a die Zahl e enthalten ist
   * @param a
   * @param e
   * @return
   */
  static boolean isIn(int[] a, int e) {
    // geht das Array durch
    for (int tmp : a) 
      // testet ob e enthalten ist
      if (tmp == e)
        return true;
    return false;
  }

  static int[] intersection(int[] a, int[] b) {
    // Fehlerfaelle aus der Aufgabe
    if (a == null || b == null || a.length == 0 || b.length == 0)
      return new int[0];

    // erzeugen ein neues Array, die Maximallaenge ist nach Aufgabe
    // min(a.length, b.length), somit ist a.length ausreichend
    int[] schnitt = new int[a.length];
    int i = 0;
    // wir muessen nur a oder b durchgehen
    for (int tmp : a) 
      // testen ob die Zahl im anderen Array enthalten ist und 
      // ob wir die Zahl nicht schon im neuen Array haben
      if (isIn(b, tmp) && !isIn(schnitt, tmp)) 
        schnitt[i++] = tmp;

    // da unser Array noch viele 0,0,0,0,0 am Ende enthaelt,
    // erstellen wir ein neues, in das alle Elemente passen
    // i war die Anzahl der Elemente
    // somit kopieren wir von 0 bis i - 1 
    int ret[] = new int[i];
    for (int j = 0 ; j < ret.length; j++)
      ret[j] = schnitt[j];

    return ret;
  }
  
  public static void main(String[] args) {
    int[] x = {2,4,3,2,7,0,2,7};
    int[] y = {7,7,8,4,2,4,3,5};
    int[] z = intersection(x, y);
    for (int i : z) {
      System.out.print(i+ ", ");
    }
  }
}
