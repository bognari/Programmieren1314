

public class Klausur030912 {
  
  /**
   * Gibt das Maximum einer Folge zurueck
   * @param folg e
   * @return
   */
  static int getMax(int[] folge) {
    int max = folge[0];
    for (int zahl : folge)
      max = max < zahl ? zahl : max;
    return max;
  }
  
  /**
   * erzeugt mit countList eine sortierte Vorkommensliste
   * @param folge
   * @return
   */
  static int[] countList(int[] folge) {
    int[] tmp = new int[getMax(folge) + 1]; // das + 1 nicht vergessen
    for (int zahl : folge) // for each Schleife
      tmp[zahl]++;
    return tmp;
  }
  
  /**
   * gibt das naechste Element zurueck
   * und verringert das jeweilige Vorkommen um eins
   * @param numberList
   * @return
   */
  static int getNext(int[] numberList) {
    int index = 0; // Position speichern
    for (int i = 0; i < numberList.length; i++) // Liste durchgehen
      if (numberList[i] > 0 && 
         (numberList[i] < numberList[index] || numberList[index] < 0)) 
    // ueberspingt alle 0 Werte und nimmt mindestens den ersten nicht 0 Wert
         index = i;
    numberList[index]--;//verringert das Vorkommen um eins(call by value-result!)
    return index;
  }
  
  static void quantitySort(int[] folge) {
    // Fehlerfaelle abfangen
    if (folge == null || folge.length == 0) throw new IllegalArgumentException();
    
    // Vorkommensliste erzeugen
    int[] tmp = countList(folge);
    
    // Array neu generieren
    for (int i = 0; i < folge.length; i++) 
      folge[i] = getNext(tmp);
    // Ausgabe war nicht gefordert
  }
  
  public static void main(String[] args) {
    int[] folge = {2, 0, 1, 0, 0, 2, 3, 1};
    quantitySort(folge);
    for (int i : folge) {
      System.out.print(i+ ", ");
    }
  }
}
