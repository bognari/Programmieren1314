
public class Klausur200312 {
  /**
   * Bildet den Absolutenbetrag einer Zahl z
   */
  static int abs(int z) {
    return z < 0 ? z * (-1) : z;
  }
     
  /** 
   * Berechnet rekuriv die Potenz einer Zahl z^e
   * @param z
   * @param e
   * @return
   */
  static int pow(int z, int e) {
    return e == 0 ? 1 : z * pow(z, --e);
  }
    
  /**
   * Gibt die Anzahl der Ziffern einer Zahl an
   * @param z
   * @return
   */
  static int length(int z) {
    int i = 1;
    while(z / pow(10,i) > 0)
      i++;
    return i;
  }
     
  /**
   * Spaltet eine Zahl in ein Array ihrer Ziffern
   * die Zahl wird dann von Hinten nach Vorne gelesen
   * @param z
   * @return
   */
  static int[] number2Array(int z) {
    int[] ret = new int[length(z)];
    for (int i = ret.length -1; i >= 0 ; i--) {
      /* 
      * Spalten die Zahl in ihre Ziffern
      * und reduzieren die Restzahl
      * wie rum wir die Ziffern speichern ist egal
      * Da unser Zahlensystem zur Basis 10 ist, 
      * ist die Rechnung relativ einfach
      */
      ret[i] = z / pow(10,i);
      z = z % pow(10,i);
    }
    return ret;
  }
   
  /**
   * Gibt an ob eine Zahl ein Palindrom ist oder nicht
   * @param z
   * @return
   */
  static boolean isPalindrom(int z) {
    int[] ziffern = number2Array(z);
    // vergleicht jede Ziffer mit der jeweilig passenden
    // koennten zwar bei der Haelfte aufhoeren aber es ist egal
    for (int i = 0; i < ziffern.length; i++)
      if (ziffern[i] != ziffern[ziffern.length - i -1])
        return false;
    return true;
  }
    
  static void palindrom(int[] a) {
    // Abfragen der Fehlerfaelle
    if (a == null || a.length == 0) throw new IllegalArgumentException();
       
    // Testen aller Zahlen, ob diese ein Palindrom ist
    for (int i : a) 
      if (isPalindrom(abs(i)))
        System.out.print(i + " ");
  }
  
  public static void main(String[] args) {
    int[] folge = {7, 23, -121, 4554, 0, 66, 31, 20};
    palindrom(folge);
  }
}
